package pro.realis.calc.display;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class Display {


    public void show(Displayable d){
        Locale locale = new Locale("en", "CA");
        //Date now = new Date();
        //DateFormat dateFormat = new SimpleDateFormat("yyyy*MM*dd**hh*z");
                //DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, locale);

        LocalTime now = LocalTime.now();
        System.out.println("[" + now.format(DateTimeFormatter.ISO_TIME) + "] displaying " + d.getDeviceName() + ": " + d.getValue());

    }

}
