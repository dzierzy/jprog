package pro.realis.calc.display;

@FunctionalInterface
public interface Displayable {

    String getValue();

    default String getDeviceName(){
        return "Unknown";
    };

}
