package pro.realis.calc.display;

import java.util.Date;

public class Clock implements Displayable {
    @Override
    public String getValue() {
        return new Date().toString();
    }

    @Override
    public String getDeviceName() {
        return "Clock";
    }
}
