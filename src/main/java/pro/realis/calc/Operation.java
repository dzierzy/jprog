package pro.realis.calc;

public interface Operation {

    double operation(double operand1, double operand2);

}
