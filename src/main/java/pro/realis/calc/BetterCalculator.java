package pro.realis.calc;


public class BetterCalculator extends Calculator {

    public BetterCalculator(){
        super(0);
    }



    @Override
    public void displayCurrentValue() {
        System.out.println("current value: " + getMemory());
    }



    public double pow(final int pow){

        /*double tmp = getMemory();
        if(pow==0){
            tmp = 1;
        } else {
            for (int i = 1; i<pow; i++){
                tmp = tmp*getMemory();
            }
        }*/

        double tmp = Math.pow(getMemory(), pow);
        setMemoryValue(tmp);
        return getMemory();
    }

}
