package pro.realis.calc;

import pro.realis.calc.display.Displayable;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Calculator class provides basic arithmetic operations.
 *
 * @author Marcin
 * @see Displayable
 * @version 0.9
 * @since 0.3
 */
public abstract class Calculator implements Displayable {



    private final static int MEMORY_SIZE = Integer.MAX_VALUE;

  /*  private double[] memory = new double[MEMORY_SIZE];

    protected int memoryIndex = -1;
*/
    private List<Double> memoryList = new ArrayList<>();


    private static int factor = 5;

    public Calculator(double initValue){
        setMemoryValue(initValue);
    }

   /* public Calculator(){
        this(0);
        System.out.println("default constructor");
    }*/


    @Override
    public String getValue(){
        //return Double.toString(getMemory());
        NumberFormat nf = NumberFormat.getInstance(Locale.CHINA);
        nf.setMaximumFractionDigits(1);
        nf.setMinimumFractionDigits(1);
        nf.setMaximumIntegerDigits(30);
        //nf.setMinimumIntegerDigits(20);
        return nf.format(getMemory());
    }

    @Override
    public String getDeviceName(){
        return "Calculator";
    }

    public double getMemory(){

        return !memoryList.isEmpty() ? memoryList.get(memoryList.size()-1) : 0;
        //return memoryIndex>=0 ? memory[memoryIndex] : Double.POSITIVE_INFINITY;
    }

    /**
     * the method presents current calculator value
     */
    public abstract void displayCurrentValue();

    public void clearMemory(){
        System.out.println("clear memory in calculator");
        //memoryIndex = 0;
        //Arrays.fill(memory, 0);
        memoryList.clear();
    }

    protected void setMemoryValue(double value){

        if(memoryList.size()==MEMORY_SIZE){
            memoryList.remove(0);
        }
        memoryList.add(value);

        /*if(memoryIndex==(memory.length-1)){
            // TODO przesuniecie
            // before - [1,2,3,4,5,6,7,8,9,10]
            // after  - [2,3,4,5,6,7,8,9,10,10]
            System.arraycopy(memory, 1, memory, 0, memory.length-1);
            //throw new CalculatorMemoryException("buffer is full");
        } else {
            memoryIndex++;
        }

        memory[memoryIndex] = value;*/
    }

    public double add(double a, double b){
        setMemoryValue(a+b);
        return getMemory();
    }


    private double doOperation(Operation op, double a){
        setMemoryValue(op.operation(getMemory(), a));
        return getMemory();
    }

    public double add(double a){

        Operation addOperation = (x, y) -> {
            System.out.println("adding " + x + " + " + y);
            return x+y;
        };
               /* new Operation() {
            @Override
            public double operation(double operand1, double operand2) {
                return operand1+operand2;
            }
        };*/
        return doOperation((x,y) -> x+y, a);
    }

    public double subtract(double a){
        return doOperation((operand1, operand2) -> operand1-operand2, a);
    }

    public double multiply(double a){
        return doOperation((operand1, operand2) -> operand1*operand2, a);
    }

    /**
     * divides by given operand. uses current value as a first operand
     *
     * @param a second operand
     * @return result of a calculation
     * @throws CalculatorException in case of zero
     */
    public double divide(double a) throws CalculatorException{
        // TODO do something with 0
        if(a==0){
            throw new CalculatorException("dzielenie przez zero");
        }
        return doOperation((operand1, operand2) -> operand1/operand2, a);
    }


    @Override
    public String toString() {
        return "Calculator{" +
                "memory=" + memoryList +
                '}';
    }

    public static int multiply(int a, int b){

        return a*b;
    }



}
