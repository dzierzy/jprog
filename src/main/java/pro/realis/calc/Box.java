package pro.realis.calc;

public class Box<T> {

    private T object;

    public Box(T object){
        this.object = object;
    }

    public T open(){
        return object;
    }
}
