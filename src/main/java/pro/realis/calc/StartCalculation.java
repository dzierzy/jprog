package pro.realis.calc;

import pro.realis.calc.display.Clock;
import pro.realis.calc.display.Display;


import java.util.*;

public class StartCalculation {

    public static void main(String[] args) {


        try {



            int arraySize = (args.length > 0) ? Integer.valueOf(args[0]) : 10;

            System.out.println("creating array of size " + arraySize);

            int[][] myArray = new int[arraySize][arraySize];

            first:
            for (int i = 1; i <= arraySize; i++) {
                second:
                for (int j = 1; j <= arraySize; j++) {
                    if (j == 13 && i == 13) continue second;
                    myArray[i - 1][j - 1] = Calculator.multiply(i, j);
                }
            }

           /* for (int[] row : myArray) {
                String rowString = Arrays.toString(row);
                System.out.println(rowString);
            }*/


            Box<Clock> boxClock = new Box<>(new Clock());
            Clock clock = boxClock.open();

            Box<Calculator> boxCalc = new Box<>(new BetterCalculator());

            Calculator calc = boxCalc.open();

            calc.add(3, 7.9);
            calc.multiply(3.2);
            calc.divide(1.2);
            calc.multiply(3.2);
            calc.divide(1);

            calc.multiply(3.2);


            ((Calculator) calc).clearMemory();

            int result = Calculator.multiply(2, 3);

            Calculator calc2 = new BetterCalculator();
            calc2.add(2.123);
            calc2.multiply(2.47);
            calc2.add(100000000);
            calc2.add(2.123);
            calc2.multiply(2.47);
            calc2.add(100000000);
            calc2.add(2.123);
            calc2.multiply(2.47);
            calc2.add(100000000);

            Display display = new Display();
            display.show(calc);
            display.show(calc2);

            display.show(() -> new Date().toString());

            System.out.println(calc2);


        } catch (CalculatorException ce) {
            System.err.println("ups! " + ce.getMessage());
        }


    }


}
