package pro.realis.xml;


import pro.realis.xml.dom.DOMTrainingParser;
import pro.realis.xml.sax.SaxTrainingParser;
import pro.realis.xml.xpath.XPathStudents;
import pro.realis.xml.xsl.XSLStudentsTransform;

/**
 * Created by marcin on 05.10.2016.
 */
public class XMLTester {
    public static void main(String[] args) throws Exception {

        System.out.println("<< DOM >>");
        new DOMTrainingParser().parseStudents();

        System.out.println("<< SAX >>");
        new SaxTrainingParser().parseStudents();

        System.out.println("<< XPATH >>");
        new XPathStudents().parseStudents();

        System.out.println("<< XSLT >>");
        new XSLStudentsTransform().transformStudents();
    }
}
