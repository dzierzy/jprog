package pro.realis.charger;

/**
 * Created by marcin on 05.04.2016.
 */
public class ChargingLauncher {

    public static void main(String[] args) {

        System.setOut(new ThreadPrintStream());

        Electricity.getInstance().turnOn();

        Charger samsungCharger = new Charger();
        Charger iphoneCharger = new Charger();

        Phone iphone = new Phone(50, "iphone8");
        Phone samsung = new Phone(75, "samsung s8");

        iphoneCharger.chargeDevice(iphone, 3);
        samsungCharger.chargeDevice(samsung, 2);

       /* try {
            Thread.sleep(5*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

        Electricity.getInstance().turnOff();
        //samsungCharger.interrupt();

       /* try {
            Thread.sleep(5*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

        Electricity.getInstance().turnOn();

        System.out.println("done.");

        Charger.es.shutdown();

    }


}
