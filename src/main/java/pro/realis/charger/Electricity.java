package pro.realis.charger;

/**
 * Created by marcin on 30.03.2017.
 */
public class Electricity {

    private static Electricity instance;

    private boolean on = false;


    private Electricity(){}

    public synchronized static Electricity getInstance(){
        if(instance==null)
           instance = new Electricity();

        return instance;
    }

    public void turnOn(){
        on = true;

        synchronized(this){
            this.notifyAll();
        }

    }

    public void turnOff(){
        on = false;
    }

    public boolean isOn() {
        return on;
    }
}
