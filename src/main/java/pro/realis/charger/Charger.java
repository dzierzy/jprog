package pro.realis.charger;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by marcin on 05.04.2016.
 */
public class Charger implements Runnable {

    public static ExecutorService es = Executors.newFixedThreadPool(10);

    private static final int POWER = 3;

    private int hours;

    private Phone p;
    

    public void chargeDevice(Phone p, int hours) {
        this.hours = hours;
        this.p = p;
        es.execute(this);
    }

    public void run(){
        p.charge(hours * POWER);
    }



}
