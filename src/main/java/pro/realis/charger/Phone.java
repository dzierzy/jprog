package pro.realis.charger;

public class Phone {

    private int battery = 50;

    private String name;

    public Phone(int battery, String name) {
        this.battery = battery;
        this.name = name;
    }

    public void charge(int circles) {
        for (int i = 1; i <= circles && !Thread.currentThread().isInterrupted(); i++) {

            if(!Electricity.getInstance().isOn()){
                System.out.println("there is no electricity");
                try {
                    synchronized(Electricity.getInstance()){
                        Electricity.getInstance().wait();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("electricity is back!");
            }

            System.out.println("charging phone " + this);
            int tmp = battery;
            try {
                Thread.sleep(3 * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            battery = tmp + 1;


        }
        System.out.println("charging finished: " + this);
    }

    @Override
    public String toString() {
        return "Phone{" +
                "battery=" + battery +
                ", name='" + name + '\'' +
                '}';
    }
}
