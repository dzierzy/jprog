package pro.realis.charger;

import java.io.OutputStream;
import java.io.PrintStream;

/**
 * Created by marcin on 07.04.2016.
 */
public class ThreadPrintStream extends PrintStream {


    public ThreadPrintStream() {
        super(System.out);
    }

    @Override
    public void println(String x) {
        super.println(Thread.currentThread().getName() + " : " + x);
    }

    @Override
    public void println(Object x) {
        println(x.toString());
    }
}
