package pro.realis.javafx.nameday;

/**
 * Created by marcin on 29.06.2016.
 */
public class NameDayInfo {

    public boolean isYourNameDay(String yourName){
        // hardcoded
        return "Adam".equalsIgnoreCase(yourName) || "Ewa".equalsIgnoreCase(yourName);
    }
}
