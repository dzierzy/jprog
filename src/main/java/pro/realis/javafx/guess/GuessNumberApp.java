package pro.realis.javafx.guess;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * Created by marcin on 29.06.2016.
 */
public class GuessNumberApp extends Application {

    private SecretNumber secret = new SecretNumber();

    private TextField guessField;

    public void start(Stage primaryStage) throws Exception {

        primaryStage.setTitle("Guess My Number");

        GridPane pane = new GridPane();
        pane.setAlignment(Pos.CENTER);
        pane.setHgap(10);
        pane.setVgap(10);
        pane.setPadding(new Insets(25, 25, 25, 25));
        ColumnConstraints column1 = new ColumnConstraints();
        column1.setHalignment(HPos.CENTER);
        pane.getColumnConstraints().add(column1);

        Scene scene = new Scene(pane, 400, 200);

        pane.add(new Text("There is a number to guess! It's bettwen 1 and 100."), 0, 0);

        Label guessLabel = new Label("Your guess: ");
        pane.add(guessLabel, 0, 1);

        guessField = new TextField();
        pane.add(guessField, 0, 2);

        guessField.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent ke) {
                if (ke.getCode().equals(KeyCode.ENTER)) {
                    GuessNumberApp.this.handle();
                    guessField.setText("");
                }
            }
        });

        Button button = new Button("Check !");
        pane.add(button, 0, 3);

        // TODO set button on action
        button.setOnAction(e->{handle();});

        primaryStage.setScene(scene);
        primaryStage.show();
    }


    private void handle(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);

        alert.setTitle("Checking result");
        alert.setHeaderText("Result is.....");
        // TODO set title
        // TODO set header
        // TODO verify number and prepare message
        // TODO set content text

        int result = secret.guess(Integer.parseInt(guessField.getText()));

        String message = result==0 ? "That's It :)" : result < 0 ? "Too big :(" : "Too little :(";


        alert.setContentText(message);
        alert.showAndWait();
    }

}
