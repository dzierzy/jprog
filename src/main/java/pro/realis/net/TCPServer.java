package pro.realis.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by marcin on 05.10.2016.
 */
public class TCPServer {

    public static void main(String[] args) {
        try {

            ExecutorService es = Executors.newFixedThreadPool(2);

            ServerSocket serverSocket = new ServerSocket(7777);

            System.out.println("[info] waiting for a client... " );
            Socket socket = serverSocket.accept();
            System.out.println("[info] connected.");

            PrintWriter writer = new PrintWriter(socket.getOutputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));


           MessageWriter mw = new MessageWriter(writer);
           es.execute(mw);

           MessageReader mr = new MessageReader(reader);
           es.execute(mr);

            es.shutdown();


            //socket.close();  // ???



        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
