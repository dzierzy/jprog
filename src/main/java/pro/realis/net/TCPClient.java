package pro.realis.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by marcin on 05.10.2016.
 */
public class TCPClient {

    public static void main(String[] args) {
        try {

            ExecutorService es = Executors.newFixedThreadPool(2);

            Socket s = new Socket("127.0.0.1", 7777);

            BufferedReader reader = new BufferedReader(new InputStreamReader(s.getInputStream()));
            PrintWriter writer = new PrintWriter(s.getOutputStream());

            MessageWriter mw = new MessageWriter(writer);
            es.execute(mw);

            MessageReader mr = new MessageReader(reader);
            es.execute(mr);


            es.shutdown();

        } catch (IOException e) {
        }
    }
}
