package pro.realis.exercises;

import java.util.Arrays;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringModification {


    public static void main(String[] args) {

        System.out.print(">");
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        System.out.println("your input: " + line);

        String[] array = new String[]{"a", "b", "c", "d"};
        String concatenated = concat(array);
        System.out.println("concatenated [" + concatenated + "]");

        String[] splitted = concatenated.split(",");
        System.out.println("splitted : " + Arrays.toString(splitted));

        StringTokenizer tokenizer = new StringTokenizer(concatenated);
        while (tokenizer.hasMoreTokens()){
            System.out.println(tokenizer.nextToken());
        }

        String patternString = "[0-9]{2}([-\\s])?[0-9]{3}";
        //String patternString =         "\\d\\d([-\\s])?\\d\\d\\d";
        String textString = "Kody pocztowe: 12-345 134567 12345";
        Pattern p = Pattern.compile(patternString);
        Matcher m = p.matcher(textString);
        while (m.find()){
            System.out.println(m.start() + "-" + m.end());
        }



    }

    private static String concat(String[] array){

        /*String result = "";
        for (String x : array ) {
            result += x;
        }
        return result;*/

        StringBuilder builder = new StringBuilder();
        for (String x : array ) {
            builder.append(x).append("! ");
        }

        return builder.toString();
    }
}
