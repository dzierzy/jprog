package pro.realis.exercises;

public enum WeekDay {



    MONDAY(1),
    TUESDAY(2),
    WEDNESDAY(3),
    THURSDAY(4),
    FRIDAY(5),
    SATURDAY(6, true),
    SUNDAY(7, true);

    public int order;

    public boolean weekend;

    private WeekDay(int order, boolean weekend){
        this.order = order;
        this.weekend = weekend;
    }

    private WeekDay(int order){
        this.order = order;
        this.weekend = false;
    }



}
