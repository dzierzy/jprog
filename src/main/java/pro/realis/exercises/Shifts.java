package pro.realis.exercises;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class Shifts {


    public static void main(String[] args) {

        Map<WeekDay,Integer> myShifts = new HashMap<>();
        myShifts.put(WeekDay.MONDAY, 1);
        myShifts.put(WeekDay.TUESDAY, 2);
        myShifts.put(WeekDay.WEDNESDAY, -1);
        myShifts.put(WeekDay.THURSDAY, 3);
        myShifts.put(WeekDay.FRIDAY, 2);
        myShifts.put(WeekDay.SATURDAY, 3);
        myShifts.put(WeekDay.SUNDAY, -1);

        System.out.println("myshifts: " + myShifts);
        int todayShift = getTodayShift(myShifts);

        System.out.println("today shift: " + todayShift);
    }

    public static int getTodayShift(Map<WeekDay, Integer> shifts){


        /*Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        int today = c.get(Calendar.DAY_OF_WEEK);
*/
        int today = LocalDate.now().getDayOfWeek().ordinal()+1;

        WeekDay weekDay = null;
        for(WeekDay day : WeekDay.values()){
            if(day.order==today){
                weekDay = day;
            }
        }

        return shifts.get(weekDay);
    }

}
