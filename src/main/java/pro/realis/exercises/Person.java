package pro.realis.exercises;

import java.io.*;
import java.time.LocalDate;
import java.util.*;
/*
CREATE TABLE PERSON(
   ID INT PRIMARY KEY,
   FIRST_NAME VARCHAR(255),
   LAST_NAME VARCHAR(255));
 */
public class Person implements Comparable<Person>, Serializable{

    private int id;

    private String firstName;

    private transient String lastName;

    public Person(int id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;


        Person person = (Person) o;

        if (firstName != null ? !firstName.equals(person.firstName) : person.firstName != null) return false;
        return lastName != null ? lastName.equals(person.lastName) : person.lastName == null;
    }

    @Override
    public int hashCode() {
        int result = firstName != null ? firstName.hashCode() : 0;
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }

    public static void main(String[] args) {
        Person p1 = new Person(1,"Jan", "Kowalski"); //11
        Person p2 = new Person(2,"Adam", "Nowak"); // 9
        Person p3 = new Person(3,"Krzysztof", "Kajak"); // 14

        Map<Person, LocalDate> vacations = new HashMap<>();
        vacations.put(p1, LocalDate.now().plusYears(1));
        vacations.put(p2, LocalDate.now().plusMonths(3));
        vacations.put(p3, LocalDate.now().plusDays(2));

        System.out.println("vacations: " + vacations);

        Person p5 = new Person(4,"Krzysztof", "Kajak");
        LocalDate date = vacations.get(p5);
        System.out.println("KK vacation: " + date);

        List<Person> persons = new ArrayList<>();
        persons.add(p2);
        persons.add(p3);
        persons.add(p1);


        //Collections.sort(persons);
        //System.out.println(persons);

        Set<Person> personsSet = new TreeSet<>((o1, o2) -> o1.firstName.length()-o2.firstName.length());
        personsSet.add(p1);
        personsSet.add(p2);
        personsSet.add(p3);

        System.out.println(personsSet);


        File f = new File("person.serialized");

        /*try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(f))){
            oos.writeObject(p3);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(f))){

            Object o = ois.readObject();
            Person deserialized = (Person) o;
            System.out.println("deserialized person : " + deserialized);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


    }

    @Override
    public int compareTo(Person o) {
        String firstAndSecond =  this.firstName + this.lastName;
        String otherFirstAndSecond = o.firstName + this.lastName;
        return otherFirstAndSecond.compareTo(firstAndSecond);
    }
}
