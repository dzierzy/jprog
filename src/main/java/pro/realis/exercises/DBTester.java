package pro.realis.exercises;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBTester {


    public static void main(String[] args) {

        System.out.println("connecting to database...");

        Person john = new Person(4, "John", "Stark");
        try (Connection connection = getConnection();) {

            System.out.println("connected? " + connection);
            PreparedStatement ps = connection.prepareStatement(
                    "insert into person (id, first_name, last_name) values (?,?,?)");

            ps.setInt(1, john.getId());
            ps.setString(2, john.getFirstName());
            ps.setString(3, john.getLastName());

            int rows = ps.executeUpdate();


            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("select id, first_name, last_name from person order by last_name desc");

            List<Person> persons = new ArrayList<>();
            while (rs.next()) {
                persons.add(
                        new Person(
                                rs.getInt("id"),
                                rs.getString("first_name"),
                                rs.getString("last_name")
                        )

                );
            }

            System.out.println(persons);

        } catch (SQLException e) {
            e.printStackTrace();
        }


    }


    private static Connection getConnection() {

        String driverName = "org.h2.Driver";
        try {
            Class.forName(driverName);
            String url = "jdbc:h2:tcp://localhost/~/abc";
            Connection connection = DriverManager.getConnection(url, "sa", "");

            return connection;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;

    }

}
