package pro.realis.exercises;

import java.io.*;

public class FileTester {


    public static void main(String[] args) {
        File dir = new File("lab");
        if(!dir.exists()){
            dir.mkdirs();
        }

        if(!dir.isDirectory()){
            throw new RuntimeException("not a folder!");
        }
        File file = new File(dir, "myfile.txt");

        if(!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        File[] files = dir.listFiles((dir1, name) -> name.endsWith(".txt"));

        for ( File f : files ) {
            System.out.println(f.getAbsolutePath() + ", " + f.isDirectory() + ", " + f.length());
        }

        // try-with-resources
        //try(BufferedWriter bw = new BufferedWriter(new FileWriter(file, false));) {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)));) {

            //bw.write("1.Zapisujemy dane do pliku. Uda sie?\n");
            //bw.write("2.Zapisujemy dane do pliku. Uda sie?");

            String line = null;
            while((line=br.readLine())!=null) {
                System.out.println("odczytano: " + line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }


    }


}
